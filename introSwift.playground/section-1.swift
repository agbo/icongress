//////////////////////////////
// Basic Building blocks
//

import Foundation

// vars and constants
var size : Double = 40
let name = "Anakin"


// Everything is an object
Int.max
Double.abs(-3)

// Conversions
let answer = Double(42)
//let sizef = Int("")


// Type alias: will be important later to avoid going crazy
// typedef <newName> = <oldName>
typealias Integer = Int
var x1: Integer = 44


// Basic Collections
var swift = "Brand new language for Apple"
swift = swift + "!"

// Array
var words = ["uno", "dos", "tres"]
words = words + ["cuatro"]

// Dictionary
var numberNames = [1: "one", 2: "two"]
numberNames[2]
numberNames[4]


// Iterate
var tally = ""
for element in words{
    tally = "\(tally) \(element)"
}
tally

for (key, value) in numberNames{
    println("\t\(key)\t\(value)")
}

// Tuples
var pair = (1, "one")

// Access
pair.0
pair.1

// They have types!
pair.0 = 33
//pair.1 = 42


// One last thing
// Iterate through ranges of numbers
for i in Range(start: 1, end: 5){
    println(i)
}

for i in 1..<5{
    println(i)
}

// Functions 101

func h(a:Int, b: Int) -> Int{
    
    return a * b
}
h(2,3)
// same internal and external name
func hh(#a:Int, #b:Int) ->Int{
    return a + b
}
hh(a: 3, b: 5)

// internal and external different names
func hhh(a:Int, b:Int, thenMultiplyBy c:Int) ->Int{
    
    return (a + b) * c
}

hhh(3, 4, thenMultiplyBy: 5)

// default values
func addSuffixTo(a:String, suffix: String = "ingly") -> String{
    return a + suffix
}

addSuffixTo("accord")
addSuffixTo("Objective", suffix: " C")

// Modifing arguments
func h3 (var a:Int, b: Int)->Int{
    a = a + 2
    return a + b
}


// Return values

func namesOfNumbers(a:Int) ->
    (Int, String, String){
    
        var val: (Int, String, String)
        
        switch a{
        case 1:
            val = (1, "one", "uno")
        case 2:
            val = (2, "two", "dos")
        
        default:
            val = (a, "Go check Google translator!", "vete a google")
            
        }
        return val
    
}
// Assignment by pattern matching
var (_, en, es) = namesOfNumbers(2)

// Higher level functions

// Functions as parameters
typealias IntToIntFunc = (Int) ->Int

func apply(f:IntToIntFunc, n:Int) -> Int{
    
    return f(n)
}

func doubler(a:Int) -> Int{
    return a * 2
}

func add42(a:Int) -> Int{
    return a + 42
}
apply(doubler, 42)
apply(add42, 3)

// functions as return values
func compose(f:IntToIntFunc, g: IntToIntFunc) -> IntToIntFunc{
    
    func comp(a:Int) -> Int{
        return f(g(a))
    }
    return comp
}

var comp = compose(add42, doubler)
comp(4)

// funcs (OF THE SAME TYPE!) inside an array
var funcs = [doubler, add42, comp]
for f in funcs{
    f(33)
}

///////////////////////
// Closures
//

"All functions are actually closures, similar to"
"blocks with a less crazy syntax"

func g(a:Int) -> Int{
    return a + 1
}
// exactly equivalent to this:
let g = {(a:Int) -> Int in
    return a + 42
}

g(1)

// they capture the lexical environment: g is captured!
let gg = {(a:Int) -> Int in return g(g(a))}
gg(34)


// you can save closures in collections just like any
// other object (type)
var closures = [ g,
    {(a:Int) ->Int in return a - 42},
    {a in return a + 45},
    {a in a / 42},
    {$0 * 42}
]

// Operators are valid closures
typealias BinaryFunc = (Int, Int) -> Int
var applier = { (f: BinaryFunc, m:Int, n:Int) -> Int
                in
                return f(m,n)
}

applier(*, 2, 4)

// trailing closure trick
func applierInv(m: Int, n:Int, f:BinaryFunc) -> Int{
    return applier(f, m, n)
}

var z = applierInv(2,4, {$0 * 2 + $1 * 3})
// equivalent to
applierInv(2, 4){
    return $0 * 2 + $1 * 3
}

////////////////////////////////////////
//
//  Optionals
//

// Box something inside an optional
var maybeAString: String? = "I'm boxed!"
var maybeAnInt: Int?

// unwrapping: 2 ways
// safe
if let certainlyAString = maybeAString{
    // if you get here it's a string
}


//println(maybeAnInt!)
println(maybeAString)


////////////////////////////////////////////////
// Decorators: functions that
// modify other functions usually to add complex
// or ugly code that you don't always need

// Logging function calls
func logger(f:IntToIntFunc) ->IntToIntFunc{
    
    return {
        (n: Int) -> Int in
        var r = f(n)
        var moment = NSDate()
        var fmt = NSDateFormatter()
        fmt.dateStyle = NSDateFormatterStyle.LongStyle
        
        println("Called at \(fmt.stringFromDate(moment)) \nInput \(n) -> \(r)")
        return r
    }
}
logger(g)(88)


// Adding pre or post conditions to a function
func precond(f: IntToIntFunc,
             cond: @autoclosure () -> Bool)
    -> IntToIntFunc{
    
        
        return {
            (n:Int) -> Int in
            var res = cond()
            assert(res, "Precondition not met. Tough luck!")
            return f(n)
        }
}

//precond(g, size < 10)(33)

/////////////////////////////
// Aggregate types: enums, structs, classes and tuples


enum LightSabreColor: Int{
    case Blue, Red, Green, Purple
}

struct LightSabre {
    
    // "class" or static property
    static let quote = " An elegant weapon for amore civilized times"
    
    // Instance properties
    var color: LightSabreColor = .Blue {
        // property observer
        willSet(newValue){
            println("About to change the color")
        }
    }
    

    var isDoubleBladed = false
    
}

// Class

class Jedi {
    
    // Stored properties
    var lightSabre = LightSabre(color: .Blue, isDoubleBladed: false)
    
    var name: String?
    var padawanOf: Jedi?
    var masterOf: Jedi?
    var midichlorians = 10
    
    //computed property
    var fullName: String{
        get{
            var full = ""
            if let theName = name{
                full = theName
            }
            if let master = padawanOf{
                full = full + " padawan of \(master)"
            }
            return full
        }
        

    }
    
    
    // initializers
    // Designated
    init(name: String?, padawanOf: Jedi?){
        self.name = name
        self.padawanOf = padawanOf
    }
    
    init(){
        name = nil
        padawanOf = nil
    }
    
    // Convenience
    convenience init(name: String){
        self.init(name: name, padawanOf: nil)
    }
    
    
    // regular methods
    func totalMidichlorians() ->Int{
        var total = midichlorians
        
        // Optional chaining
        if let parentalMidichlorians = padawanOf?.midichlorians{
            total = midichlorians + parentalMidichlorians
        }
        return total
    }
    
}

// Inheritance

class Sith: Jedi {
    
    // if you don0t write any designated initializers
    // you inherit those of the super class
    override init(){
        super.init()
        lightSabre.color = .Red
        lightSabre.isDoubleBladed = true
    }
    
    
    
}



// Extensions

typealias Euro = Double
extension Euro{
    var €: Double {return self}
    var $: Double {return self * 1.3}
}

var total = 123.€ + 45.56.$

typealias Task = ()->()
extension Int{
    func times(task:Task){
        for i in 1...self{
            task()
        }
    }
}
4.times({println("My name is Groot!")})


// Protocols


struct EvenNumbers: GeneratorType {
    var current = 0
    mutating func next() -> Int?{
        current = current + 2
        return current
    }
    
}
var evens = EvenNumbers()
evens.next()
evens.next()



// Access control
class ExtraTerrestrialPlant{
    private func speak() -> String{
        return "I am Groot"
    }
}

var groot = ExtraTerrestrialPlant()
groot.speak()






























